package com.fish.blog.service;

import com.fish.blog.entity.CommentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface ICommentService {
    Page<CommentEntity> findByPostId(String postId, PageRequest pageRequest);
    CommentEntity saveComment(CommentEntity commentEntity);
    List<CommentEntity> findCommentByUserIdAndPaging(Integer userId, Integer start, Integer limit);
    Long countByUserId(Integer userId);
    void delete(Integer commentId);
}