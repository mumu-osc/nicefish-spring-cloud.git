package com.fish.blog.service.impl;

import com.fish.blog.entity.CommentEntity;
import com.fish.blog.repository.CommentRepository;
import com.fish.blog.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements ICommentService {
    @Autowired
    private CommentRepository commentRepository;

    @Override
    @Cacheable(value = "comments",key = "#postId.toString().concat('-comments')")
    public Page<CommentEntity> findByPostId(String postId, PageRequest pageRequest) {
        return commentRepository.findByPostId(postId,pageRequest);
    }

    @Override
    @CacheEvict(value = "comments",allEntries = true)
    public CommentEntity saveComment(CommentEntity commentEntity) {
        return commentRepository.save(commentEntity);
    }

    @Override
    public List<CommentEntity> findCommentByUserIdAndPaging(Integer userId, Integer start, Integer limit) {
        return commentRepository.findCommentByUserIdAndPaging(userId,start,limit);
    }

    @Override
    public Long countByUserId(Integer userId) {
        return commentRepository.countByUserId(userId);
    }

    @Override
    @CacheEvict(value = "comments",allEntries = true)
    public void delete(Integer commentId) {
        commentRepository.delete(commentId);
    }
}
